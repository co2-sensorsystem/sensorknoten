//////////////////////////////////////////////////////////////////////////////////////////////////
// PINS
//////////////////////////////////////////////////////////////////////////////////////////////////

/// Analouge pin for voltage measurement
#define PIN_VOLTAGE     A0

// SCL and SDA pins for SCD30
#define PIN_SCD_SCL     D1
#define PIN_SCD_SDA     D2

// RX and TX pins for SDS011
#define PIN_SDS_RX      D3
#define PIN_SDS_TX      D4

// LEDs for traffic light
#define PIN_LED_RED     D5
#define PIN_LED_YELLOW  D6
#define PIN_LED_GREEN   D7