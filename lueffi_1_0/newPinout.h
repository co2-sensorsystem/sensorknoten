//////////////////////////////////////////////////////////////////////////////////////////////////
// PINS
//////////////////////////////////////////////////////////////////////////////////////////////////

/// Analouge pin for voltage measurement
#define PIN_VOLTAGE     A0

// SCL and SDA pins for SCD30
#define PIN_SCD_SCL     D3
#define PIN_SCD_SDA     D4

// RX and TX pins for SDS011
#define PIN_SDS_RX      3 //RX PIN
#define PIN_SDS_TX      1 //TX PIN

// LEDs for traffic light
#define PIN_LED_RED     D0
#define PIN_LED_YELLOW  D1
#define PIN_LED_GREEN   D2