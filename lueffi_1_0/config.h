/*****************************************
 * WIFI                                  *
 *****************************************/

// define WIFI settings
#ifndef STASSID
// The SSID of the WIFI to connect to
#define STASSID "Lüffi-Netzwerk"
// The password of the WIFI to connect to
#define STAPSK  "DatasinkToSensorNode"
#endif


/*****************************************
 * IP-adress                             *
 *****************************************/

// the IP-address of the server for communication
IPAddress remoteServerIP(10,42,0,1);


/*****************************************
 * pinout                                *
 *****************************************/

/*
 * Choose a pinout for the sensor node.
 * oldPinout: pinout for prototype
 * newPinout: pinout for new board and sensor node
 */
#include "newPinout.h"


/*****************************************
 * traffic light                         *
 *****************************************/

// 1: for UBA and old traffic light limits of system
// 2: for new traffic light limits from BA
#define choosenco2Limits 2



/*****************************************
 * batteries                             *
 *****************************************/

// 4 or 8 batteries?
 #define usedBatteries 8
 

/*****************************************
 * SDS011                                *
 *****************************************/

#define isAvailableSDS011 true

/*
 * Set factor value for PM2.5 and/or PM10 if needed a calibration.
 * If the offset is "1", the PM values of SDS011 will be unchanged.
 * Range: 0 < x
 * Examples: "0.94", "1.23"
 */
#define SDS011_PM25_FACTOR 1
#define SDS011_PM10_FACTOR 1
