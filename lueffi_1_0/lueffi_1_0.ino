/* the sensor nodes ip can be recieved with netcat , command
 *  nc -ul 8889
 *  while in wifiMode the sensor node can be instructed to make a measurement using 
 *  nc -u [IP of sensor node] 8888
 */

/*
 * Source for Voltage Readout
 * https://ezcontents.org/esp8266-battery-level-meter Accessed 06.06.2021
 */

 /*
  * Source for sensor SCD30
  * https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library Accessed 06.06.2022
  */

 /*
  * Source for sensor SDS011
  * https://github.com/lewapek/sds-dust-sensors-arduino-library Accessed 11.05.2022
  */

   /*
  * Source for DoubleResetDetector
  * https://github.com/datacute/DoubleResetDetector Accessed 12.06.2022
  */

#include <DoubleResetDetector.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "SparkFun_SCD30_Arduino_Library.h"
#include <IPAddress.h>
#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "SdsDustSensor.h"

// all configurationes the user can do on sensor node initial
#include "config.h"

// Number of seconds after reset during which a reset will be considered a double reset
#define DRD_TIMEOUT 10
// RTC Memory Address for the DoubleResetDetector to use
#define DRD_ADDRESS 0

#define NO_DATA -9999

#define LED_ON "on"
#define LED_OFF "off"

#define SDS011_ON "on"
#define SDS011_OFF "off"

// possible messages at wifi mode and error for local problems at sensor node:
enum Message { startMeasurement, aliveping, stopMeasurement, trafficLight, switchFineDustSensor, forcedCalibration, error };
Message calledMessage = error;

bool isMessage = false;


/*****************************************
 * variables for comminucation to server *
 *****************************************/

// local port to listen on for UDP-server functionality
unsigned int localPort = 8888;
int remoteServerPort = 33333;

// buffer to hold incoming packet,
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1];

WiFiUDP Udp;

// upon reset this value determines if wifi or default-mode is entered
bool wifiActive = false;
bool loggedIn = false;


/*****************************************
 * SCD30                                 *
 *****************************************/

// The SCD30 sensor (co2, temperature, relative humidity)
SCD30  scd30;

/**
 * define interval for default and wifi mode and deepsleep
 * (for energy efficiency)
 * one interval for default and wifi mode
 * seconds between measurements of scd30: 
 * 2 to 1800 (30 minutes), stored in non-volatile memory of SCD30
 */
const int scd30MeasurementIntervalInMode = 2;
const int scd30MeasurementIntervalInDeepsleep = 1000; // seconds > 1000 not works, highes possible value?


/*****************************************
 * SDS011                                *
 *****************************************/

// The SDS011 sensor (PM2.5, PM10)
SdsDustSensor sds(PIN_SDS_RX, PIN_SDS_TX);

/**
 * Set SDS011 sleep at start.
 * Variable workingStateOfSDS011 saves the current state of SDS011,
 * sleep or work.
 * Needed later to set sleep or wakeup the SDS011 correct.
 */
WorkingStateResult workingStateOfSDS011 = sds.sleep();

// do measurment in SDS011OffMode, SDS011 is sleeping, if true 
bool isSDS011OffMode = true;


/*****************************************
 * variables for measurement             *
 *****************************************/

// measurement values of SCD30
float co2 = NO_DATA;
float temperature = NO_DATA;
float humidity = NO_DATA;

// measurement values of SDS011
float pm25 = NO_DATA;
float pm10 = NO_DATA;


/*****************************************
 * variables for traffic light           *
 *****************************************/

// CO2 limits for the steps of the traffic light
int co2Limit1;
int co2Limit2;

// do measurment in phantomMode, no LED shines from traffic light, if true 
bool isPhantomMode = false;


/*****************************************
 * variables for voltage readout         *
 *****************************************/

int nVoltageRaw;
float fVoltage = NO_DATA;
int lowVoltageCounter;
int lastVoltageCheck = -1;

float voltageGood;
float voltageLow;
float voltageConnected;

float commingVoltageToBoard;
float voltagevar;


/*****************************************
 * Reset                                 *
 *****************************************/
DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS);


/*****************************************
 * setup                                 *
 *****************************************/

void setup() {
	pinMode(PIN_LED_RED, OUTPUT); //red led
	pinMode(PIN_LED_YELLOW, OUTPUT); //yellow led
	pinMode(PIN_LED_GREEN, OUTPUT); //green led
	pinMode(LED_BUILTIN, OUTPUT); //status led
	Serial.begin(9600);
  Serial.println("sensor node starting");
  
  // start SCD30, if doesn't runs, than don't start, because 
  // sensor node should be a "CO2-Ampel"
  Wire.begin(PIN_SCD_SDA, PIN_SCD_SCL);
	if (!scd30.begin()) {
		Serial.println("Failed to find SCD30 chip");
		while (1) { delay(5); }
	} else {
    Serial.println("SCD30 runs");    
  }
  
  // start and configured the SDS011, if available
  delay(800);
  if (isAvailableSDS011) {
    Serial.println("");
    //Serial.println(sds.queryFirmwareVersion().toString()); // prints firmware version
    Serial.println("Try to start SDS011");
    sds.begin();
    Serial.println("SDS011 runs");

    /**
     * Set reporting mode to "query".
     * This mode allowed the SDS011 to set sleep or wakeup.
     * Only in this mode the SDS011 gets request and 
     * gives respones.
     */
    Serial.println(sds.setQueryReportingMode().toString());

    // set SDS011 sleep at start
    workingStateOfSDS011 = sds.sleep();

  } else {
    Serial.println("For this sensor node is no SDS011 available");
  }

  Serial.println("change mode");
	// Connection to WLAN is established if the controller was reset in a short timespan before this reset
	if (drd.detectDoubleReset()) {
    wifiActive = false;
	} else {
		wifiActive = true;
		Serial.println("Entering WIFI-Mode");
		WiFi.mode(WIFI_STA);
		WiFi.begin(STASSID, STAPSK);
		while (WiFi.status() != WL_CONNECTED) {
			Serial.print('.');
			delay(500);
		}
		Serial.print("Connected! IP address: ");
		Serial.println(WiFi.localIP());
		//launches a UDP-server on port 8888
		Udp.begin(localPort);
  }
  Serial.println("");
  
  /*
   * set measurement interval of SCD30, 
   * if not scd30MeasurementIntervalInMode
   */
  setSCD30MeasurementInterval(scd30MeasurementIntervalInMode);
  
  // set traffic light limits
  if (choosenco2Limits == 1) {
    co2Limit1 = 1000;
    co2Limit2 = 2000;
  } else if (choosenco2Limits == 2) {
    co2Limit1 = 800;
    co2Limit2 = 1100;
  } else {
    // if nothing correct, take old limits
    co2Limit1 = 1000;
    co2Limit2 = 2000;
  }

  // set voltage limits for sensor node with SDS011 or without and for battery count
  if (usedBatteries == 8 || isAvailableSDS011) {
    commingVoltageToBoard = 5.0;
    voltagevar = 2.80;
    voltageGood = 10;
    voltageLow = 9.6;
    voltageConnected = 7;
  } else { // usedBatteries == 4 || !isAvailableSDS011
    commingVoltageToBoard = 3.3;
    voltagevar = 1.79;
    voltageGood = 5;
    voltageLow = 4.8;
    voltageConnected = 3;
  }
}

/*****************************************
 * loop                                  *
 *****************************************/

void loop() {

  checkBatteryVoltage();
	
	if (wifiActive) {
    // if sensor node is connected with wifi

    if (loggedIn) {
      wifiMode();
    } else {
      logIn();
    }
	} else {
		defaultMode();
	}
}

/**
 * defaultMode
 * Makes measurements every 2 seconds and updates the LED traffic light accordingly.
 * At these state the sensor node works only as "CO2-Ampel".
 */
void defaultMode() {
  loggedIn = false;
  
  /**
   * default mode doesn't needed values of PM, 
   * because there output only on serial monitor and not for "normal" users.
   * Set SDS011 sleeping to save energy of batteries.
   */
  setSDS011sleeping();

  digitalWrite(LED_BUILTIN, LOW);
  if (readSensorData()) {
    Serial.println("Data available!");

    // Turns the led according to co2 value on
    ledTrafficLight();

  } else {
    Serial.println("No data");
  }
  // short blinking of internal LED indicates that default-mode is active
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
      // 2 seconds pause to wait for new data
      // 1 second is not enough when requesting all data from senor which causes it to be not ready in the next loop
  delay(2000);
}

/**
 * wifiMode
 * Connects to a WiFi network, registers with a data sink / control server and waits for commands:
 * - send measurement data
 * - send ping telling the device is online and listening for commands
 * - stop measurement for sensor node
 * - set power state of SDS011
 * - set power state of LED traffic light
 * - calibrate sensors with given values
 */
void wifiMode() {
	// Transmits last measurement if prompted via udp-packet. Give answer for request of other requests. 
  
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.println(WiFi.macAddress());
    isMessage = true;

    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println(packetBuffer);
    DynamicJsonDocument messageJson(UDP_TX_PACKET_MAX_SIZE + 1);
    DynamicJsonDocument answerJson(UDP_TX_PACKET_MAX_SIZE + 1);
    
    deserializeJson(messageJson, packetBuffer);
    // memset(packetBuffer, ' ', sizeof(packetBuffer));
    // std::fill(&packetBuffer, packetBuffer + sizeof(packetBuffer), 0);
    
    calledMessage = resolveMessage(messageJson["event"]);

    switch (calledMessage) {

      // If Json message contains startMeasurement, the measurment will be started
      case startMeasurement:
        // Sensor node shouldn't change in defaultMode at running measurement, so the reset-function will be blocked.
        drd.stop();
        Serial.println("Reset blocked");

        if (!isSDS011OffMode) {
          setSDS011working();
        }

        readSensorData();
        ledTrafficLight();

        // if the json message contains the event: startMeasurment, it will also include a timestamp
        // which will be part of the answerJson
        answerJson["battery"] = fVoltage;
        answerJson["co2"] = co2;
        answerJson["temperature"] = temperature;
        answerJson["humidity"] = humidity;
        answerJson["pm25"] = pm25;
        answerJson["pm10"] = pm10;
        answerJson["mac"] = WiFi.macAddress();
        answerJson["timestamp"] = messageJson["timestamp"];
        sendToServer(answerJson);
        break;
      // end of case startMeasurement

      // if the json message event contains aliveping, send a answer with alive and mac adress
      case aliveping:
        answerJson["event"] = "alive";
        answerJson["battery"] = fVoltage;
        Serial.println(fVoltage);
        sendToServer(answerJson);
        break;
      // end of case aliveping
      
      case stopMeasurement:
        // set traffic light off to reduce energy consumption
        if (isPhantomMode == false) {
          setOffLedTrafficLight();
          Serial.println("traffic-light out");
          Serial.println("");
        }

        if (isSDS011OffMode == false) {
          // set SDS011 sleeping to reduce energy consumption
          setSDS011sleeping();
        }       

        // inform the server about stooped measurement at sensor node
        answerJson["event"] = "stopped";
        sendToServer(answerJson);

        // allow reset and change to defaultMode
        drd.loop();
        break;
      // end of case stopMeasurement

      case switchFineDustSensor:
        if (isAvailableSDS011) {
          if (messageJson.containsKey("fineDustSensor")) {
            if (strcmp(messageJson["fineDustSensor"], SDS011_ON) == 0) {
              isSDS011OffMode = false;
              setSDS011working();
              answerJson["status"] = "ok";
              Serial.println("SDS011 on");
            } else if (strcmp(messageJson["fineDustSensor"], SDS011_OFF) == 0) {
              isSDS011OffMode = true;
              setSDS011sleeping();
              answerJson["status"] = "ok";
              Serial.println("SDS011 off");
            } else {
              answerJson["status"] = "error";
              answerJson["data"] = "Did not recognize value for SDS011";
            }
          } else {
            answerJson["status"] = "error";
            answerJson["data"] = "Message did not contain value for key 'SDS011'";          
            Serial.println("Error activity of SDS011 couldn't change");
          }
        } else {
          answerJson["status"] = "ok";
          answerJson["data"] = "This sensor node has no fine dust sensor";          
          Serial.println("No switch of SDS011 sensor, because this sensor node has no fine dust sensor");
        }
        
        Serial.println("");
        
        // inform the server about the changed activity of the SDS011
        answerJson["event"] = "switchFineDustSensor";
        sendToServer(answerJson);
        break;
      // end of case switchFineDustSensor
      
      case trafficLight:
        if (messageJson.containsKey("trafficLight")) {
          if (strcmp(messageJson["trafficLight"], LED_ON) == 0) {
            isPhantomMode = false;
            ledTrafficLight();
            answerJson["status"] = "ok";
            Serial.println("traffic-light on");
          } else if (strcmp(messageJson["trafficLight"], LED_OFF) == 0) {
            isPhantomMode = true;
            setOffLedTrafficLight();
            answerJson["status"] = "ok";
            Serial.println("traffic-light off");
          } else {
            answerJson["status"] = "error";
            answerJson["data"] = "Did not recognize value for trafficLight";
          }
        } else {
          answerJson["status"] = "error";
          answerJson["data"] = "Message did not contain value for key 'trafficLight'";          
          Serial.println("Error traffic-light couldn't change");
        }
        Serial.println("");
        
        // inform the server about the changed state of the traffic-light
        answerJson["event"] = "trafficLight";
        sendToServer(answerJson);
        break;
      // end of case phantomMode

      case forcedCalibration:
        bool calibrationSuccessful = true;
        String answerData = "";
        Serial.println("calibration");
        if (messageJson.containsKey("co2")) {
          int co2CalibrationValue = int(messageJson["co2"]);
          Serial.print("forced calibration of CO2 value with " + String(co2CalibrationValue) + "ppm");
          scd30.setForcedRecalibrationFactor(co2CalibrationValue);
          uint16_t s;
          calibrationSuccessful = scd30.getForcedRecalibration(&s);
        }

        if (messageJson.containsKey("temperature")) {
          float targetTemperature = float(messageJson["temperature"]);
          Serial.println("forced calibration of temperature value with " + String(targetTemperature));
          // ensure to have current temperature data
          if (readSCD30SensorData()) {
            // calculate the offset; the target temperature is given by the datasink
            Serial.print("current temperature: ");
            Serial.println(temperature);
            Serial.print("target temperature: ");
            Serial.println(targetTemperature);
            float temperatureDelta = (temperature - targetTemperature) + scd30.getTemperatureOffset();
            Serial.print("Setting temperature offset to ");
            Serial.println(temperatureDelta);
            scd30.setTemperatureOffset(temperatureDelta);
          } else {
            Serial.println("Did not modify the temperature offset, because getting current measurement data failed");
            calibrationSuccessful = false;
            answerData = "Failed to set temperature offset, because getting the current measurement data failed";
          }
        }
        
        // inform the server about the calibration success
        answerJson["event"] = "forcedCalibration";
        if (calibrationSuccessful) {
          answerJson["status"] = "ok";
        } else {
          answerJson["status"] = "error";
          answerJson["data"] = answerData; 
        }
        sendToServer(answerJson);
        break;
      // end of case forcedCalibration
      
    }
  } else {
    if (isMessage) {
      Serial.println(WiFi.macAddress());
      Serial.println("Waitning for incomming message");
      Serial.println("");
    }
    isMessage = false;
  }
}

void logIn() {
  Serial.println("Sending login message to server");
  // the local IP of the controller is sent to the server to register
  Udp.beginPacket(remoteServerIP, remoteServerPort);
  Udp.write(("{\"event\": \"login\", \"mac\": \"" + WiFi.macAddress() + "\"}").c_str());
  Udp.endPacket();
  unsigned int counter = 0;
  // wait for response by the server for 5 seconds
  while (!loggedIn && counter++ != 50) {
    int packetSize = Udp.parsePacket();
    if (packetSize) {
      Serial.println();
      Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
      Serial.println(packetBuffer);
      DynamicJsonDocument messageJson(UDP_TX_PACKET_MAX_SIZE + 1);
      DynamicJsonDocument answerJson(UDP_TX_PACKET_MAX_SIZE + 1);
    
      deserializeJson(messageJson, packetBuffer);
      //memset(packetBuffer, 0, sizeof(packetBuffer));

      if (strcmp(messageJson["event"], "response") == 0
          && strcmp(messageJson["messageId"], "login") == 0) {
        // the server sent a response to the previously sent login message
        // therefore the sensor node is considered logged in
        loggedIn = true;
        isPhantomMode = strcmp(messageJson["LED"], LED_OFF) == 0;
        isSDS011OffMode = strcmp(messageJson["fineDustSensor"], SDS011_OFF) == 0;
        Serial.println("Connected to server");
      }
    } else {
      delay(100);
      Serial.print(".");
    }
  }
  Serial.println();
}

bool readSensorData() {
  bool isSCD30Data = false;
  bool isSDS011Data = false;
  if (readSCD30SensorData()) {
    isSCD30Data = true;
  } 
  if (readSDS011SensorData()) {
    isSDS011Data = true;
  }
  Serial.println("");
  if (!isSCD30Data && !isSDS011Data) {
    return false;
  }
  return true;
}

bool readSCD30SensorData() {
  if (!scd30.dataAvailable()){
      Serial.println("Error reading sensor data from SCD30");
      Serial.println("");
      co2 = NO_DATA;
      temperature = NO_DATA;
      humidity = NO_DATA;
      return false;
    } else {
      co2 = scd30.getCO2();
      temperature = scd30.getTemperature();
      humidity = scd30.getHumidity();
    }

    Serial.print("CO2: ");
    Serial.print(co2);
    Serial.println(" ppm");

    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println(" degrees C");

    Serial.print("Relative Humidity: ");
    Serial.print(humidity);
    Serial.println(" %");
    return true;
}

bool readSDS011SensorData() {
  // handle data from SDS011, if sensor is available
  if (isAvailableSDS011) {
    if (workingStateOfSDS011.isWorking()) {
        /**
        * Set reporting mode to query, to get PM values.
        * Normally setting of mode in setup should be enough.
        */
        sds.setQueryReportingMode();
        // ervery time this output:
        // Mode: undefined
        // should be output:
        // Mode: query
        //Serial.println(sds.setQueryReportingMode().toString());

        // get current values of SDS011, have to be in mode "query"
        PmResult pm = sds.queryPm();
        // check if in response are correct PM values
        if (pm.isOk()) {
          pm25 = pm.pm25;
          pm10 = pm.pm10;

          calibrateSDS011();

          Serial.print("PM2.5 = ");
          Serial.print(pm25);
          Serial.println(" micrograms per cubic meter"); //µg/m^3");
          Serial.print("PM10 = ");
          Serial.print(pm10);
          Serial.println(" micrograms per cubic meter"); //µg/m^3");
          return true;

        } else {
          Serial.print("Could not read values from sensor SDS011, reason: ");
          Serial.println(pm.statusToString());
          pm25 = NO_DATA;
          pm10 = NO_DATA;
          return false;
        }   
      } else {
        Serial.println("SDS011 is sleeping, so no values for PM25 and PM10");
        pm25 = NO_DATA;
        pm10 = NO_DATA;
        return false;
      }
  } else {
    Serial.println("No SDS011 available, so no values for PM25 and PM10");
    pm25 = NO_DATA;
    pm10 = NO_DATA;
    return false;
  }
}

/**
 * Set a new measurement interval for SCD30, if the new value doesn't used.
 * @param scd30MeasurementInterval - the new interval for the SCD30 as Integer.
 */
void setSCD30MeasurementInterval(int scd30MeasurementInterval) {
  if (scd30.getMeasurementInterval() != scd30MeasurementInterval) {
    scd30.setMeasurementInterval(scd30MeasurementInterval);
    Serial.print("New measurement intervall for SCD30: ");
    int interval = scd30.getMeasurementInterval();
    Serial.print(interval);
    Serial.println(" seconds");
    delay(200);
  } 
}

// Multiply factor for PM2.5 and/or PM10.
void calibrateSDS011() {
  if (SDS011_PM25_FACTOR != 1) {
    pm25 *= SDS011_PM25_FACTOR;
    Serial.print("Multiplyed factor for PM2.5 with: ");
    Serial.print(SDS011_PM25_FACTOR);
    Serial.println(" micrograms per cubic meter"); //µg/m^3");
  }
  if (SDS011_PM10_FACTOR != 1) {
    pm10 *= SDS011_PM10_FACTOR;
    Serial.print("Multiplyed factor for PM10 with: ");
    Serial.print(SDS011_PM10_FACTOR);
    Serial.println(" micrograms per cubic meter"); //µg/m^3");
  }  
}

// Set the SDS011 sleeping, if he is working.
void setSDS011sleeping() {
  if (isAvailableSDS011 && workingStateOfSDS011.isWorking()) {
    workingStateOfSDS011 = sds.sleep();
    Serial.println("SDS011 is sleeping");
  }
}

// Wakeup the SDS011 and set him working, if he is sleeping.
void setSDS011working() {
  if (isAvailableSDS011 && !workingStateOfSDS011.isWorking()) {
    workingStateOfSDS011 = sds.wakeup();
    Serial.println("SDS011 is working");
  }
}

void ledTrafficLight() {
  if (!isPhantomMode) {
  	if (co2 < co2Limit1) {
  		//turn on green led
  		digitalWrite(PIN_LED_RED, LOW);
  		digitalWrite(PIN_LED_YELLOW, LOW);
  		digitalWrite(PIN_LED_GREEN, HIGH);
  	} else if (co2 >= co2Limit1 && co2 <= co2Limit2) {
  		//turn on yellow led
  		digitalWrite(PIN_LED_RED, LOW);
  		digitalWrite(PIN_LED_YELLOW, HIGH);
  		digitalWrite(PIN_LED_GREEN, LOW);
  	} else {
  		//turn on red led
  		digitalWrite(PIN_LED_RED, HIGH);
  		digitalWrite(PIN_LED_YELLOW, LOW);
  		digitalWrite(PIN_LED_GREEN, LOW);
  	}
  } else {
    // turn all led off
    setOffLedTrafficLight();
  }
}

void setOffLedTrafficLight() {
  // turn all led off
  digitalWrite(PIN_LED_RED, LOW);
  digitalWrite(PIN_LED_YELLOW, LOW);
  digitalWrite(PIN_LED_GREEN, LOW);
}

/**
 * Returns the giving message as needed type Message.
 * @param message
 */
Message resolveMessage(std::string message) {
      if (message == "startMeasurement") return startMeasurement;
      if (message == "aliveping") return aliveping;
      if (message == "stopMeasurement") return stopMeasurement;
      if (message == "trafficLight") return trafficLight;
      if (message == "switchFineDustSensor") return switchFineDustSensor;
      if (message == "forcedCalibration") return forcedCalibration;
      return error;
}

void checkBatteryVoltage() {
  if (lastVoltageCheck > millis() - 2000 && lastVoltageCheck > -1) {
    // do not run this check everytime, at most every 2 seconds to not put the ESP into deep sleep if the voltage is low for a second.
    return;
  }
  
  lastVoltageCheck = millis();
  
	// The variable lowVoltageCounter is used to catch drops in voltage that happen from time to time. 
	// The variable is increased by 1 every time the voltage measured in 1 cycle is to low and decreased by 1 if the measured voltage is ok if lowVoltageCounter > 0
	// This is supposed to catch those drops and to make sure, that the node doesn't shut down over time as the value would add up
	// This however may cause damage to the batterys if this cycle is bruteforced and/or ignored, as the sensor is not affected by it.
  // When the SDS011 is performing a measurement, the nVoltageRaw value is decreased significantly (~30).
	nVoltageRaw = analogRead(PIN_VOLTAGE);
	fVoltage = (commingVoltageToBoard / 1024.0) * nVoltageRaw * voltagevar;

	if (fVoltage >= voltageGood) {
		if (lowVoltageCounter > 0) {
			// Decrease Counter if Voltage is ok so it doesn't shut down over time while still being properly charged
			lowVoltageCounter = lowVoltageCounter - 1;
		}
		Serial.println("Voltage: " + String(fVoltage));
    Serial.println("");
	} else if (fVoltage > voltageLow) {
		Serial.println("Warning - Battery Level Low!");
		Serial.println("Voltage: " + String(fVoltage));
    Serial.println("");
		if (lowVoltageCounter > 0) {
			// Decrease Counter if Voltage is ok so it doesn't shut down over time while still being properly charged
			lowVoltageCounter = lowVoltageCounter - 1;
		}
  } else if (fVoltage < voltageConnected) {
    // When powered via USB, calculating the voltage does not work.
    // The calculated value depends on the way the components are aranged and welded.
    Serial.println("Probably connected via USB");
    Serial.println("Voltage: " + String(fVoltage));
    Serial.println("");
	} else {
    // Example: four 1.2V batteries should not go below 4.8V
    // if they do, this can trigger deep dechargning which hurts battery life time
		Serial.println("Warning - Battery Level Critical!");
		Serial.println("Warning - System will be put in sleep!");
		Serial.println("Voltage: " + String(fVoltage));
    Serial.println("");
		lowVoltageCounter = lowVoltageCounter + 1;
		if (lowVoltageCounter > 15) {
      // before deepsleep, set SDS011 sleeping
      setSDS011sleeping();
      // before deepsleep, set measurement interval to the largest possible value
      setSCD30MeasurementInterval(scd30MeasurementIntervalInDeepsleep);
      Serial.println("System is sleeping");
			ESP.deepSleep(0);
		}
	}
}

/**
 * Send a message to the server's UDP socket.
 * The messages to the server are using the JSON format.
 * @param doc - the JSON document to be send to the backend. This method add the "mac" property for easier identification of the sensor node.
 */
void sendToServer(DynamicJsonDocument doc) {
  doc["mac"] = WiFi.macAddress();
  serializeJson(doc, Udp);
  Udp.beginPacket(remoteServerIP, remoteServerPort);
  Udp.println();
  Udp.endPacket();
}